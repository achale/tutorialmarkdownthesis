---
output: 
  pdf_document:
    number_sections: true
geometry: "left=3.8cm,right=2.5cm,top=2.5cm,bottom=2.5cm"
papersize: a4
header-includes:
  \usepackage{amsmath}
  \usepackage{pdflscape}
bibliography: bibliography.bib
csl: vancouver-imperial-college-london.csl
---



```{r, include=FALSE, echo=FALSE}
  library(ggplot2)
  library(kableExtra)
```




\pagenumbering{roman}     <!-- first page with Roman numbering -->

\newpage                  <!-- new page -->

\begin{center}          % <!-- center text -->

\LARGE{My Report or Thesis Title} % <!-- make large text -->

\bigskip                % <!-- blank lines -->
\bigskip

\large{Dr Hale}

\end{center}              <!-- end center -->

\newpage                 <!-- end page -->  

\begin{center}

\large{Abstract}

\end{center}

\bigskip

...this is the abstract text...

\newpage 

\tableofcontents

\newpage
# List of tables{-}
\renewcommand{\listtablename}{} <!-- removes default section name -->
\listoftables

\newpage
# List of figures{-}
\renewcommand{\listfigurename}{}
\listoffigures

\newpage

\pagenumbering{arabic} 

# Introduction{#intro}

This is my first citation @hale2014.

**Hello, how are you?** *I am ok, thank you!*

\newpage 

# Methods{#meths}

## Equations
The energy in the rest frame of the mass is given by Equation \ref{eq:myEqLabel}.

\begin{equation}
\label{eq:myEqLabel}
E = m_0 c^2
\end{equation}

Alternatively the relativistic equation $E^2=(pc)^2+(m_0c^2)^2$ relates energy to momentum and rest mass.


## Bullet lists

My list of unordered points are as follows:

* item A
    * sub-item A
    * sub-item B
* item B
* item C


My list of ordered points are as follows:

1. item A
    * sub-item A
    * sub-item B
2. item B
    a. sub-item A
    b. sub-item B
3. item C

\newpage 

# Results{#res}

## R-code chunk

Run a linear model (*lm*) on the data *iris* data using the following code chunk.

```{r, echo=FALSE, include=TRUE, message=FALSE, warning=FALSE}
  data(iris)
  fit = lm( formula = Petal.Width ~ Petal.Length,  data = iris)
  summary(fit)
```


## R-code inline

The number of iris species is `r length(unique(iris$Species))`.


## Images

See Figure \ref{img:butterfly} for image of Painted Lady!

```{r, include=TRUE, echo=FALSE, message=FALSE, warning=FALSE, fig.pos="h!", fig.align="center", out.height="25%", fig.cap="Painted Lady butterfly copyright Alison Hale\\label{img:butterfly}"}
  knitr::include_graphics("paintedLadyCopyrightAlisonHale.png")
```


## Figures{#resultsFigs}

Histograms of iris petal width are given in Figures \ref{fig:hist1} and \ref{fig:hist2}.

```{r, include=TRUE, echo=FALSE, message=FALSE, warning=FALSE, fig.height=3, fig.cap="First histogram of petal width\\label{fig:hist1}"}
  hist(iris$Petal.Width, breaks=seq(0,2.5,by=0.25), xlab="petal width", ylab="count", main="")
```

```{r, include=TRUE, echo=FALSE, message=FALSE, warning=FALSE, fig.height=3, fig.cap="Second histogram of petal width\\label{fig:hist2}"}
ggplot(data=iris, aes(x=Petal.Width) ) + 
        geom_histogram(breaks=seq(0,2.5,by=0.25), color="black", aes(fill=..count..)) +
        scale_fill_continuous("Count", type="gradient") +
        theme_light(base_size=8) +
        labs(title="", x="\npetal width", y="count\n")
```


Scatter plots iris petal dimensions are given in Figures \ref{fig:scat1} and \ref{fig:scat2}.

```{r, include=TRUE, echo=FALSE, message=FALSE, warning=FALSE, fig.height=3, fig.cap="First scatter plot of petal width and height\\label{fig:scat1}"}
  plot(x=iris$Petal.Width, y=iris$Petal.Length, xlab="width", ylab="height")
```

```{r, include=TRUE, echo=FALSE, message=FALSE, warning=FALSE, fig.height=3, fig.cap="Second scatter of petal width and height\\label{fig:scat2}"}
ggplot(data=iris, aes(x=Petal.Width, y=iris$Petal.Length) ) + 
    geom_point( size=1, colour="#000080" ) +
    theme_light(base_size=8) +
    labs(title="", x="\nwidth", y="height\n")
```

## Tables{#resultsTables}

The first ten lines of the iris data can be found in Table \ref{tbl:iris}.

```{r, include=TRUE, echo=FALSE, message=FALSE}
  kable(iris[1:10,], "latex", escape=FALSE, booktabs=TRUE, linesep="", caption="Iris data\\label{tbl:iris}") %>%
    kable_styling(latex_options=c("HOLD_position"))
```


Landscape table using the first twenty lines of the iris data can be found on the next page in Table \ref{tbl:irisLand}.

```{r, include=TRUE, echo=FALSE, message=FALSE}
  kable(t(iris[1:20,]), "latex", escape=FALSE, booktabs=TRUE, linesep="", caption="Iris data\\label{tbl:irisLand}") %>%
    kable_styling(latex_options=c("HOLD_position"), font_size=7) %>%
    landscape() 
```
 

# Conclusion{#con}
We described methods in Section \ref{meths} and plotted the results in Section \ref{resultsFigs}.
Supplementary material is in Appendix \ref{app:mat}.

\newpage 

# References{-}
<div id="refs"></div>

\newpage 

\renewcommand{\thesection}{\Alph{section}}
\setcounter{section}{1}

# Appendix{-}

## Landscape pages{#app:mat}

Figure \ref{fig:figLand} is on a landscape page.

\begin{landscape}

```{r, include=TRUE, echo=FALSE, message=FALSE, warning=FALSE, fig.height=5.2, fig.width=9.5, fig.align="center", fig.cap="petal versus septal width\\label{fig:figLand}"}

  plot(x=iris$Petal.Width, y=iris$Sepal.Width, xlab="petal", ylab="sepal")

```

\end{landscape}

## Supporting code



